#include <iostream>
#include <string>

using namespace std;

int main()
{
    /* Este programa divide una cadena de numeros en la cantidad que ingrese el usuario, si no se presta para hacer grupos enteros,
    se agrega un cero a la izquierda, por medio de comprobaciones de While y verificando que la cantidad de numeros sea divisible entre
    el numero ingresado por el usuario, luego recorre la cadena y reune los primeros N numeros para ser sumados, y asi sucesivamente con los
    numeros siguientes.*/

    int divisorNumero,cantNumeros,sumaTotal=0;
    string cadena,sumaDividida;
    cout << "Ingrese una cadena de caracteres numericos: " << endl;
    cin >> cadena;
    cout << "Ingrese el numero por el que dividira la cadena: "<<endl;
    cin >> divisorNumero;

    cantNumeros= cadena.length();  //Se obtiene la cantidad de numeros ingresados

    while (cantNumeros % divisorNumero != 0){   //Si no es division entera, se agrega el 0 antes de la cadena ingresada hasta que sea entera
        cadena= "0"+cadena;
        cantNumeros= cadena.length();   //Se actualiza el valor de la cantidad de numeros
    }

    for (int i=0;i < cantNumeros;){     //Ciclo que recorre TODOS los numeros
        sumaDividida="";        //Suma donde se concatenan los N numeros encontrados
        for (int c=0; c < divisorNumero;c++,i++){   //Ciclo que recorre los N numeros siguientes a i
            sumaDividida+=cadena[i];
        }
        sumaTotal+=atoi (sumaDividida.c_str());     //Se convierte la concatenacion de los N numeros en entero y se le suma a la total
    }
    cout <<"Original: "<<cadena<<endl;
    cout <<"Suma: "<<sumaTotal<<endl;



    return 0;
}
